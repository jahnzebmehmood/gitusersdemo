package com.demo.demoapplication.connectivity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.demo.demoapplication.controllers.AppClass
import java.util.ArrayList

object InternetConnectionListener {

    private var internetStatusReceiver: BroadcastReceiver? = null
    private var listenerList: ArrayList<OnInternetConnectionListener>? = null

    fun addInternetReceiver(connectionListener: OnInternetConnectionListener) {
        if (listenerList == null)
            listenerList = ArrayList()
        listenerList?.add(connectionListener)
        val internetStatusIntents = IntentFilter()
        internetStatusIntents.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        if (internetStatusReceiver == null) {
            internetStatusReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {

                    if (listenerList != null) {
                        for (listener in listenerList!!) {
                            listener.onConnectionChanged()
                        }
                    }

                }
            }
        }
        AppClass.getInstance().registerReceiver(internetStatusReceiver, internetStatusIntents)
    }

    fun removeReceiver(internetStatusReceiver: OnInternetConnectionListener) {
        if (listenerList != null && listenerList!!.contains(internetStatusReceiver)) {
            listenerList?.remove(internetStatusReceiver)
        }
        if (listenerList != null && listenerList!!.size == 0) {
            unregisterReceiver()
        }
    }

    fun unregisterReceiver() {
        try {
            AppClass.getInstance().unregisterReceiver(internetStatusReceiver)
        } catch (ignore: Exception) {
        }

    }

    interface OnInternetConnectionListener {
        fun onConnectionChanged()
    }

}