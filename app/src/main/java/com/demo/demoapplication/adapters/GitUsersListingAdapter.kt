package com.demo.demoapplication.adapters

import android.content.Context
import android.graphics.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.demo.demoapplication.databinding.ItemGitUserBinding
import com.demo.demoapplication.interfaces.GitItemClickListener
import com.demo.demoapplication.models.gituser.GitHubUser
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import java.lang.Exception

class GitUsersListingAdapter(context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var gitItemClickListener: GitItemClickListener? = null
    var gitListing = ArrayList<GitHubUser>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ItemGitUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UsersListingViewHolder(binding.root, binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = gitListing[position]
        when(holder){
            is UsersListingViewHolder -> {
                
                Picasso.get().load(item?.avatarUrl).into(holder.itemGitUserBinding.ivProfilePhoto, object :
                    Callback {
                    override fun onSuccess() {
                        val originalBitmap = holder.itemGitUserBinding.ivProfilePhoto.drawable.toBitmap()
                        val invertedBitmap = holder.createInvertedBitmap(holder.itemGitUserBinding.ivProfilePhoto.drawable.toBitmap())
                        if (position%4 == 0) {
                            holder.itemGitUserBinding.ivProfilePhoto.setImageBitmap(invertedBitmap)
                        }else{
                            holder.itemGitUserBinding.ivProfilePhoto.setImageBitmap(originalBitmap)
                        }
                    }
                    override fun onError(e: Exception?) {
                    }
                })
                holder.itemGitUserBinding.tvTitle.text = item.login
                holder.itemGitUserBinding.tvSubTitle.text = item.type
                holder.itemGitUserBinding.root.setOnClickListener {
                    gitItemClickListener?.onItemClick(item)
                }
                holder.itemGitUserBinding.ivNotes.setOnClickListener {
                    if (holder.itemGitUserBinding.crdSearchItem.visibility == View.VISIBLE){
                        item.note = holder.itemGitUserBinding.etSearch.text.toString().trim()
                        gitItemClickListener?.updateNote(item, position)
                        holder.itemGitUserBinding.crdSearchItem.visibility = View.GONE
                    }else{
                        holder.itemGitUserBinding.crdSearchItem.visibility = View.VISIBLE
                        item.note?.let {
                            holder.itemGitUserBinding.etSearch.setText(it)
                        }
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return gitListing.size
    }


    inner class UsersListingViewHolder(view: View,var itemGitUserBinding: ItemGitUserBinding): RecyclerView.ViewHolder(view){
        fun createInvertedBitmap(src: Bitmap): Bitmap? {
            val colorMatrix_Inverted = ColorMatrix(
                floatArrayOf(
                    -1f,
                    0f,
                    0f,
                    0f,
                    255f,
                    0f,
                    -1f,
                    0f,
                    0f,
                    255f,
                    0f,
                    0f,
                    -1f,
                    0f,
                    255f,
                    0f,
                    0f,
                    0f,
                    1f,
                    0f
                )
            )
            val ColorFilter_Sepia: ColorFilter = ColorMatrixColorFilter(colorMatrix_Inverted)
            val bitmap = Bitmap.createBitmap(
                src.width, src.height,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            val paint = Paint()
            paint.colorFilter = ColorFilter_Sepia
            canvas.drawBitmap(src, 0f, 0f, paint)
            return bitmap
        }
    }


}