package com.demo.demoapplication.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.demo.demoapplication.databinding.ItemLoadingStateBinding

class ListingLoadStateAdapter(private  val retry:() -> Unit): LoadStateAdapter<ListingLoadStateAdapter.LoadStateViewHolder>() {


    class LoadStateViewHolder(view: View, val itemLoadingStateBinding: ItemLoadingStateBinding): RecyclerView.ViewHolder(view)

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        holder.itemLoadingStateBinding.btnRetry.isVisible = loadState !is LoadState.Loading
        holder.itemLoadingStateBinding.tvLoadingError.isVisible = loadState !is LoadState.Loading
        holder.itemLoadingStateBinding.progressBar.isVisible = loadState is LoadState.Loading

        if (loadState is LoadState.Error){
            holder.itemLoadingStateBinding.tvLoadingError.text = loadState.error.localizedMessage
        }

        holder.itemLoadingStateBinding.btnRetry.setOnClickListener {
            retry.invoke()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        val binding = ItemLoadingStateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LoadStateViewHolder(binding.root, binding)
    }
}