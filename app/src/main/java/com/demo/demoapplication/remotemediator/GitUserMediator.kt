package com.demo.demoapplication.remotemediator

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.delivery.dynamicdelivery.constants.AppContants
import com.delivery.dynamicdelivery.network.ApiRepositry
import com.demo.demoapplication.models.RemoteKeys
import com.demo.demoapplication.models.gituser.GitHubUser
import com.demo.demoapplication.storage.AppLocalStorage
import retrofit2.HttpException
import java.io.IOException
import java.io.InvalidObjectException

@ExperimentalPagingApi
class GitUserMediator(private val apiService: ApiRepositry,private val appDatabase: AppLocalStorage) :
    RemoteMediator<Int, GitHubUser>() {

    override suspend fun load(
        loadType: LoadType, state: PagingState<Int, GitHubUser>
    ): MediatorResult {

        val pageKeyData = getKeyPageData(loadType, state)
        val page = when (pageKeyData) {
            is MediatorResult.Success -> {
                return pageKeyData
            }
            else -> {
                pageKeyData as Int
            }
        }

        try {
            val response = apiService.getGitUsersList(page, state.config.pageSize)
            val isEndOfList = if(response?.data == null){
                true
            }else{
                (response.data as ArrayList<GitHubUser>).isEmpty()
            }
            val gitList = response?.data?.let {
                it as ArrayList<GitHubUser>
                appDatabase.withTransaction {
                    // clear all tables in the database
                    if (loadType == LoadType.REFRESH) {
                        appDatabase.gitRemoteDao().clearRemoteKeys()
                        appDatabase.gitHubUserDao().clearAll()
                    }
                    val prevKey = if (page == AppContants.STARTING_PAGE_INDEX) null else page - 1
                    val nextKey = if (isEndOfList) null else page + 1
                    val keys = it.map {
                        RemoteKeys(repoId = it.id, prevKey = prevKey, nextKey = nextKey)
                    }
                    appDatabase.gitRemoteDao().insertAll(keys)
                    appDatabase.gitHubUserDao().insertAll(it)
                }
            }
            return MediatorResult.Success(endOfPaginationReached = isEndOfList)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    /**
     * this returns the page key or the final end of list success result
     */
    suspend fun getKeyPageData(loadType: LoadType, state: PagingState<Int, GitHubUser>): Any? {
        return when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getClosestRemoteKey(state)
                remoteKeys?.nextKey?.minus(1) ?: AppContants.STARTING_PAGE_INDEX
            }
            LoadType.APPEND -> {
                state.lastItemOrNull()
                    ?: return MediatorResult.Success(endOfPaginationReached = true)
                val remoteKeys = getLastRemoteKey(state)
//                    ?: throw InvalidObjectException("Remote key should not be null for $loadType")
                getRemoteKeys()?.nextKey
            }
            LoadType.PREPEND -> {
                val remoteKeys = getFirstRemoteKey(state)
//                    ?: throw InvalidObjectException("Invalid state, key should not be null")
                //end of list condition reached
                remoteKeys?.prevKey ?: return MediatorResult.Success(endOfPaginationReached = true)
                remoteKeys.prevKey
            }
        }
    }

    /**
     * get the last remote key inserted which had the data
     */
    private suspend fun getLastRemoteKey(state: PagingState<Int, GitHubUser>): RemoteKeys? {
        return state.pages
            .lastOrNull { it.data.isNotEmpty() }
            ?.data?.lastOrNull()
            ?.let { gitHubUser -> appDatabase.gitRemoteDao().remoteKeysGitUserId(gitHubUser.id) }
    }

    /**
     * get the first remote key inserted which had the data
     */
    private suspend fun getFirstRemoteKey(state: PagingState<Int, GitHubUser>): RemoteKeys? {
        return state.pages
            .firstOrNull() { it.data.isNotEmpty() }
            ?.data?.firstOrNull()
            ?.let { gitHubUser -> appDatabase.gitRemoteDao().remoteKeysGitUserId(gitHubUser.id) }
    }

    /**
     * get the closest remote key inserted which had the data
     */
    private suspend fun getClosestRemoteKey(state: PagingState<Int, GitHubUser>): RemoteKeys? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                appDatabase.gitRemoteDao().remoteKeysGitUserId(repoId)
            }
        }
    }

    private suspend fun getRemoteKeys(): RemoteKeys? {
        return appDatabase.gitRemoteDao().getRedditKeys().firstOrNull()
    }

}