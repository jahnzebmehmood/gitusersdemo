package com.demo.demoapplication.converters

import androidx.room.TypeConverter
import com.demo.demoapplication.models.gituser.GitHubUser
import com.google.gson.Gson

class Converters {

    @TypeConverter
    fun listToJson(value: List<GitHubUser>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<GitHubUser>::class.java).toList()
}