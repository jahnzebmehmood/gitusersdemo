package com.demo.demoapplication.pagingsource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.delivery.dynamicdelivery.constants.AppContants.PAGE_SIZE
import com.delivery.dynamicdelivery.constants.AppContants.STARTING_PAGE_INDEX
import com.delivery.dynamicdelivery.network.ApiRepositry
import com.demo.demoapplication.models.gituser.GitHubUser
import com.demo.demoapplication.storage.GitHubUserDao
import java.io.IOException

class GitPagingSourceWithLocalDB() : PagingSource<Int, GitHubUser>() {
    override fun getRefreshKey(state: PagingState<Int, GitHubUser>): Int? {
        TODO("Not yet implemented")
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GitHubUser> {
        try {
            return try {
                val pageIndex = params.key ?: STARTING_PAGE_INDEX
                val gitUsersResponse = ApiRepositry.getGitUsersList(pageIndex, PAGE_SIZE)
                var gitUserList = gitUsersResponse?.data?.let {
                    it as ArrayList<GitHubUser>
                }?: kotlin.run {
                    ArrayList<GitHubUser>()
                }
                val nextKey =
                    if (gitUserList.isEmpty())  {
                        null
                    } else {
//                            gitUserList.forEach {
//                                AppClass.getRoomDataBaseInstance(AppClass.getInstance()).gitHubUserDao().insertAll(it)
//                            }
                        pageIndex + 1
                    }
                LoadResult.Page(
                    data = gitUserList,
                    prevKey = if (pageIndex == STARTING_PAGE_INDEX) null else pageIndex,
                    nextKey = nextKey
                )
            }catch (exception: IOException) {
                return LoadResult.Error(exception)
            }
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }
}