package com.demo.demoapplication.pagingsource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.delivery.dynamicdelivery.constants.AppContants.PAGE_SIZE
import com.delivery.dynamicdelivery.constants.AppContants.STARTING_PAGE_INDEX
import com.delivery.dynamicdelivery.models.common.BaseResponse
import com.delivery.dynamicdelivery.network.ApiRepositry
import com.demo.demoapplication.controllers.AppClass
import com.demo.demoapplication.models.gituser.GitHubUser
import com.demo.demoapplication.models.gituser.GitUserSearchResponse
import java.io.IOException

class GitUserSearchPagingSource(private val query: String) : PagingSource<Int, GitHubUser>() {
    override fun getRefreshKey(state: PagingState<Int, GitHubUser>): Int? {
        TODO("Not yet implemented")
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GitHubUser> {
        try {
            return try {
                val pageIndex = params.key ?: STARTING_PAGE_INDEX
                val gitUsersResponse = ApiRepositry.searchGitUsersList(query, pageIndex, PAGE_SIZE)
                val gitUserList = (gitUsersResponse?.data as GitUserSearchResponse).items
                val nextKey =
                    if (gitUserList?.isEmpty() == true) {
                        null
                    } else {
                        pageIndex + 1
                    }
                LoadResult.Page(
                    data = gitUserList!!,
                    prevKey = if (pageIndex == STARTING_PAGE_INDEX) null else pageIndex,
                    nextKey = nextKey
                )
            }catch (exception: IOException) {
                return LoadResult.Error(exception)
            }
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }
}