package com.demo.demoapplication.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.delivery.dynamicdelivery.network.ApiRepositry
import com.demo.demoapplication.controllers.AppClass
import com.demo.demoapplication.models.gituser.GitHubUser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class GitUserDetailViewModel: ViewModel() {

    val apiResponse = MutableLiveData<Any>()
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default
    private val scope = CoroutineScope(coroutineContext)

    fun gitUserDetail(userName: String){
        scope.launch {
            val gitUserDetailResponse = ApiRepositry.getGitUserDetail(userName)
            if (gitUserDetailResponse?.data is GitHubUser) {
                apiResponse.postValue(gitUserDetailResponse.data)
            } else if (gitUserDetailResponse?.errorResponse != null) {
                apiResponse.postValue(gitUserDetailResponse.errorResponse)
            }

        }
    }


}