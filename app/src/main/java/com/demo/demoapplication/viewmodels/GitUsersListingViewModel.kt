package com.demo.demoapplication.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.delivery.dynamicdelivery.constants.AppContants
import com.delivery.dynamicdelivery.network.ApiRepositry
import com.demo.demoapplication.controllers.AppClass
import com.demo.demoapplication.models.gituser.GitHubUser
import com.demo.demoapplication.pagingsource.GitUserSearchPagingSource
import com.demo.demoapplication.pagingsource.GitUsersPagingSource
import com.demo.demoapplication.remotemediator.GitUserMediator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class GitUsersListingViewModel: ViewModel() {

    val apiResponse = MutableLiveData<Any>()
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default
    private val scope = CoroutineScope(coroutineContext)

    private val appDataBase = AppClass.getRoomDataBaseInstance(AppClass.getInstance())
    private val apiService = ApiRepositry // as im getting direst repository so i don't need to call api service here...

    fun getDefaultPageConfig(): PagingConfig {
        return PagingConfig(pageSize = AppContants.PAGE_SIZE, enablePlaceholders = true)
    }

    @ExperimentalPagingApi
    fun loadUserFromDb(pagingConfig: PagingConfig = getDefaultPageConfig()): Flow<PagingData<GitHubUser>> {
        if (appDataBase == null) throw IllegalStateException("Database is not initialized")

        val pagingSourceFactory = { appDataBase.gitHubUserDao().getAllUsers() }
        return Pager(
            config = pagingConfig,
            pagingSourceFactory = pagingSourceFactory,
            remoteMediator = GitUserMediator(apiService, appDataBase)
        ).flow
    }



    val assignedList: Flow<PagingData<GitHubUser>> = Pager(PagingConfig(pageSize = 50)) {
        GitUsersPagingSource()
    }.flow
        .cachedIn(viewModelScope)

    fun getSearchedList(query: String): Flow<PagingData<GitHubUser>>{
        return Pager(PagingConfig(pageSize = 10)) {
            GitUserSearchPagingSource(query)
        }.flow
            .cachedIn(viewModelScope)
    }

    fun gitUsersList(){
        scope.launch {
            val gitUsersListFromLocalDb = ApiRepositry.getGitUsersListFromDb()
            if (gitUsersListFromLocalDb.size > 0){
                apiResponse.postValue(gitUsersListFromLocalDb)
            }else{
                val gitUsersResponse = ApiRepositry.getGitUsersList()
                if (gitUsersResponse?.data is ArrayList<*>) {
                    val gitUserList = gitUsersResponse.data as ArrayList<GitHubUser>
                    apiResponse.postValue(gitUserList)
                    gitUserList.forEach {
                        AppClass.getRoomDataBaseInstance(AppClass.getInstance()).gitHubUserDao().insertAll(it)
                    }

                } else if (gitUsersResponse?.errorResponse != null) {
                    apiResponse.postValue(gitUsersResponse.errorResponse)
                }
            }
        }
    }
}