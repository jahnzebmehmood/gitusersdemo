package com.demo.demoapplication.models.gituser

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class GitUserSearchResponse(
    @ColumnInfo(name = "total_count")
    @field:SerializedName("total_count")
    val totalCount: Int? = null,
    @ColumnInfo(name = "incomplete_results")
    @field:SerializedName("incomplete_results")
    val incompleteResults: Boolean? = false,
    @ColumnInfo(name = "items")
    @field:SerializedName("items")
    val items: ArrayList<GitHubUser>? = null
)