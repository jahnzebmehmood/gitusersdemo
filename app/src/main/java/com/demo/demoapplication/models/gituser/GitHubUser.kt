package com.demo.demoapplication.models.gituser

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class GitHubUser(
    @PrimaryKey
    @field:SerializedName("id")
    val id: Int,
    @ColumnInfo(name = "avatar_url")
    @field:SerializedName("avatar_url")
    val avatarUrl: String? = null,
    @ColumnInfo(name = "bio")
    @field:SerializedName("bio")
    val bio: String?= null,
    @ColumnInfo(name = "blog")
    @field:SerializedName("blog")
    val blog: String?= null,
    @ColumnInfo(name = "collaborators")
    @field:SerializedName("collaborators")
    val collaborators: Int?= null,
    @ColumnInfo(name = "company")
    @field:SerializedName("company")
    val company: String?= null,
    @ColumnInfo(name = "created_at")
    @field:SerializedName("created_at")
    val createdAt: String?= null,
    @ColumnInfo(name = "disk_usage")
    @field:SerializedName("disk_usage")
    val diskUsage: Int?= null,
    @ColumnInfo(name = "email")
    @field:SerializedName("email")
    val email: String?= null,
    @ColumnInfo(name = "events_url")
    @field:SerializedName("events_url")
    val eventsUrl: String?= null,
    @ColumnInfo(name = "followers")
    @field:SerializedName("followers")
    val followers: Int?= null,
    @ColumnInfo(name = "followers_url")
    @field:SerializedName("followers_url")
    val followersUrl: String?= null,
    @ColumnInfo(name = "following")
    @field:SerializedName("following")
    val following: Int?= null,
    @ColumnInfo(name = "following_url")
    @field:SerializedName("following_url")
    val followingUrl: String?= null,
    @ColumnInfo(name = "gists_url")
    @field:SerializedName("gists_url")
    val gistsUrl: String?= null,
    @ColumnInfo(name = "gravatar_id")
    @field:SerializedName("gravatar_id")
    val gravatarId: String?= null,
    @ColumnInfo(name = "hireable")
    @field:SerializedName("hireable")
    val hireable: Boolean?= null,
    @ColumnInfo(name = "html_url")
    @field:SerializedName("html_url")
    val htmlUrl: String?= null,
    @ColumnInfo(name = "location")
    @field:SerializedName("location")
    val location: String?= null,
    @ColumnInfo(name = "login")
    @field:SerializedName("login")
    val login: String?= null,
    @ColumnInfo(name = "name")
    @field:SerializedName("name")
    val name: String?= null,
    @ColumnInfo(name = "node_id")
    @field:SerializedName("node_id")
    val nodeId: String?= null,
    @ColumnInfo(name = "organizations_url")
    @field:SerializedName("organizations_url")
    val organizationsUrl: String?= null,
    @ColumnInfo(name = "owned_private_repos")
    @field:SerializedName("owned_private_repos")
    val ownedPrivateRepos: Int?= null,
//    @ColumnInfo
//    @field:SerializedName("plan")
//    val plan: Plan?= null,
    @ColumnInfo(name = "private_gists")
    @field:SerializedName("private_gists")
    val privateGists: Int?= null,
    @ColumnInfo(name = "public_gists")
    @field:SerializedName("public_gists")
    val publicGists: Int?= null,
    @ColumnInfo(name = "public_repos")
    @field:SerializedName("public_repos")
    val publicRepos: Int?= null,
    @ColumnInfo(name = "received_events_url")
    @field:SerializedName("received_events_url")
    val receivedEventsUrl: String?= null,
    @ColumnInfo(name = "repos_url")
    @field:SerializedName("repos_url")
    val reposUrl: String?= null,
    @ColumnInfo(name = "site_admin")
    @field:SerializedName("site_admin")
    val siteAdmin: Boolean?= null,
    @ColumnInfo(name = "starred_url")
    @field:SerializedName("starred_url")
    val starredUrl: String?= null,
    @ColumnInfo(name = "subscriptions_url")
    @field:SerializedName("subscriptions_url")
    val subscriptionsUrl: String?= null,
    @ColumnInfo(name = "total_private_repos")
    @field:SerializedName("total_private_repos")
    val totalPrivateRepos: Int?= null,
    @ColumnInfo(name = "twitter_username")
    @field:SerializedName("twitter_username")
    val twitterUsername: String?= null,
    @ColumnInfo(name = "two_factor_authentication")
    @field:SerializedName("two_factor_authentication")
    val twoFactorAuthentication: Boolean?= null,
    @ColumnInfo(name = "type")
    @field:SerializedName("type")
    val type: String?= null,
    @ColumnInfo(name = "updated_at")
    @field:SerializedName("updated_at")
    val updatedAt: String?= null,
    @ColumnInfo(name = "url")
    @field:SerializedName("url")
    val url: String?= null,
    @ColumnInfo(name = "note")
    @field:SerializedName("note")
    var note: String?= null

)