package com.delivery.dynamicdelivery.models.common



class DialogConfig(){
    var icon: Int = 0
    var iconUrl:String?=""
    var title: String ?= ""
    var titleColor: Int = 0
    var titleTextAllCaps: Boolean = false
    var showEditText: Boolean = false
    var titleTextSize: Int = 0
    var titleTextFont: Int = 0

    var message: String ?= ""
    var messageTextColor: Int = 0
    var messageTextSize: Int = 0
    var messageTextFont: Int = 0

    var primaryBtnText: String ?= ""
    var primaryBtnTextColor: Int = 0
    var primaryBtnBgDrawable: Int = 0
    var primaryBtnTextSize: Int = 0

    var secondaryBtnText: String ?= ""
    var secondaryBtnTextColor: Int = 0
    var secondaryBtnBgDrawable: Int = 0
    var secondaryBtnTextSize: Int = 0

//    var btnPrimaryClickListener: DialogCommonMessage.DialogPrimaryEventListener? = null
//    var btnSecondaryClickListener: DialogCommonMessage.DialogSecondaryEventListener ? = null

    var dialogBgColor: Int = 0
}