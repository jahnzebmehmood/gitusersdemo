package com.delivery.dynamicdelivery.models.common

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class LocationAddressModel : Parcelable {
    var address:String?=null
    var city:String?=null
    var state:String?=null
    var country:String?=null
    var postalCode:String?=null
    var knownName:String?=null
}