package com.demo.demoapplication.models.gituser


import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class Plan(
    @ColumnInfo
    @field:SerializedName("collaborators")
    val collaborators: Int? = null,
    @ColumnInfo
    @field:SerializedName("name")
    val name: String?= null,
    @ColumnInfo
    @field:SerializedName("private_repos")
    val privateRepos: Int?= null,
    @ColumnInfo
    @field:SerializedName("space")
    val space: Int?= null
)