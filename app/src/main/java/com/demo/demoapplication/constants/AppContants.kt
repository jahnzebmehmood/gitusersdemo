package com.delivery.dynamicdelivery.constants

object AppContants {
    const val PREFERENCE_NAME="demo_app"
    const val USER_NAME = "user_name"
    var STARTING_PAGE_INDEX = 0
    var PAGE_SIZE = 50
}