package com.delivery.dynamicdelivery.constants

object ApiEndpoints {
    const val BASE_URL="https://api.github.com/"

    const val USER="user"
    const val USERS="users"
    const val SEARCH="search/users"
}