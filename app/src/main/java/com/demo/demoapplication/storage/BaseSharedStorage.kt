package com.theentertainerme.storage

import android.content.Context
import android.content.SharedPreferences
import com.demo.demoapplication.controllers.AppClass


open class BaseSharedStorage(preferenceName:String){

    var mSharedPreferences: SharedPreferences? = null

    init {
        mSharedPreferences = AppClass.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
    }


    @Synchronized
    fun saveString(key: String, value: String) {
        mSharedPreferences?.edit()?.putString(key, value)?.apply()
    }

    @Synchronized
    fun saveLong(key: String, value: Long) {
        mSharedPreferences?.edit()?.putLong(key, value)?.apply()
    }


    @Synchronized
    fun getLong(key: String, default: Long): Long {
        return mSharedPreferences?.getLong(key, default)!!
    }

    @Synchronized
    fun getString(key: String, default: String): String {
        return mSharedPreferences?.getString(key, default)!!
    }

    @Synchronized
    fun saveInt(key: String, value: Int) {
        mSharedPreferences?.edit()?.putInt(key, value)?.apply()
    }

    fun getInt(key: String, default: Int): Int {
        return mSharedPreferences?.getInt(key, default)!!
    }

    @Synchronized
    fun saveBoolean(key: String, value: Boolean) {
        mSharedPreferences?.edit()?.putBoolean(key, value)?.apply()
    }

    @Synchronized
    fun getBoolean(key: String, default: Boolean): Boolean {
        return mSharedPreferences?.getBoolean(key, default)!!
    }


    @Synchronized
    fun saveFloat(key: String, value: Float) {
        mSharedPreferences?.edit()?.putFloat(key, value)?.apply()
    }



    @Synchronized
    fun getFloat(key: String, default: Float): Float {
        return mSharedPreferences?.getFloat(key, default)!!
    }

    @Synchronized
    fun removeKey(key: String) {
        mSharedPreferences?.edit()?.remove(key)?.commit()!!
    }
    @Synchronized
    fun hasKey(key: String): Boolean {
        return mSharedPreferences?.contains(key)?:false
    }
    fun getSharedEditor(): SharedPreferences.Editor? {
        return mSharedPreferences?.edit()
    }

}