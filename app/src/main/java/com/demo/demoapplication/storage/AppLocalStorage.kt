package com.demo.demoapplication.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.demo.demoapplication.models.RemoteKeys
import com.demo.demoapplication.models.gituser.GitHubUser

@Database(entities = [GitHubUser::class, RemoteKeys::class], version = 2)
abstract class AppLocalStorage:  RoomDatabase(){
    abstract fun gitHubUserDao(): GitHubUserDao
    abstract fun gitRemoteDao(): RemoteKeysDao
}