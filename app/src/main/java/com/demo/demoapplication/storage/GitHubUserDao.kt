package com.demo.demoapplication.storage

import androidx.paging.PagingSource
import androidx.room.*
import com.demo.demoapplication.models.gituser.GitHubUser

@Dao
interface GitHubUserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(users: List<GitHubUser>)

    @Query("Select * from githubuser")
    fun getAllUsers(): PagingSource<Int, GitHubUser>

    @Query("Select * from githubuser")
    suspend fun getAllUsersList(): List<GitHubUser>

    @Insert
    fun insertAll(vararg gitHubUser: GitHubUser)

    @Query("SELECT * FROM githubuser WHERE name LIKE :query")
    fun pagingSource(query: String): PagingSource<Int, GitHubUser>


    @Query("DELETE FROM githubuser WHERE name = :query")
    suspend fun deleteByQuery(query: String)

    @Query("SELECT * FROM githubuser WHERE id IN (SELECT id FROM githubuser ORDER BY RANDOM() LIMIT :size) ")
    abstract suspend fun getRandomPosts(size: Int): List<GitHubUser>

    @Query("DELETE FROM githubuser")
    suspend fun clearAll()

    @Update
    fun updateUser(vararg gitHubUser: GitHubUser)
}