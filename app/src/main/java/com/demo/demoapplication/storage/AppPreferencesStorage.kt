package com.delivery.dynamicdelivery.storage

import com.delivery.dynamicdelivery.constants.AppContants
import com.demo.demoapplication.models.gituser.GitHubUser
import com.google.gson.Gson
import com.theentertainerme.storage.BaseSharedStorage

object AppPreferencesStorage :  BaseSharedStorage(AppContants.PREFERENCE_NAME){

    const val KEY_USER_DATA = "user_data"

    fun saveUserSession(user: GitHubUser?){
        saveString(KEY_USER_DATA,Gson().toJson(user))
    }

    fun removeSession(){
        removeKey(KEY_USER_DATA)
    }


    fun getUserSession():GitHubUser?{
        val userResponseString= getString(KEY_USER_DATA,"")
        val userResponse = Gson().fromJson(userResponseString, GitHubUser::class.java)
        return userResponse
    }
}