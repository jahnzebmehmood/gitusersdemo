package com.demo.demoapplication.controllers

import android.app.Application
import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.demo.demoapplication.storage.AppLocalStorage

class AppClass : Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
    }

    companion object {
        lateinit var context: Context

        fun getInstance(): Context {
            return context
        }

        fun getRoomDataBaseInstance(context: Context): AppLocalStorage{
          return  Room.databaseBuilder(
                context,
                AppLocalStorage::class.java, "git_database"
            ).build()
        }


    }
}