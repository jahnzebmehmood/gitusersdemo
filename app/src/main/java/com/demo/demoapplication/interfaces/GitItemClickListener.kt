package com.demo.demoapplication.interfaces

import com.demo.demoapplication.models.gituser.GitHubUser

interface GitItemClickListener {

    fun onItemClick(gitHubUser: GitHubUser?)
    fun updateNote(gitHubUser: GitHubUser?, position: Int)
}