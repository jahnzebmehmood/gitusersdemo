package com.theentertainerme.network.base

import com.delivery.dynamicdelivery.utils.Utils
import com.demo.demoapplication.controllers.AppClass
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject


object RequestCommonParameters {


    fun setCommonParamsToJson(commonParamsJson: JSONObject, isEncrypted: Boolean = false): JSONObject {
        commonParamsJson.put("device_id", Utils.getDeviceUDID(AppClass.getInstance()))
        commonParamsJson.put("language","en")
//        if (AppPreferencesStorage.getLat() != 0.0){
//            commonParamsJson.put("lat",AppPreferencesStorage.getLat())
//            commonParamsJson.put("lng",AppPreferencesStorage.getLng())
//        }

        return commonParamsJson
    }



    fun postJsonBodyRequest(jsonString: String, isEncrypted: Boolean): RequestBody {
        val jsonObject = JSONObject(jsonString)
        val encryptedJSON = RequestCommonParameters.setCommonParamsToJson(jsonObject, isEncrypted)
        val body = RequestBody.create("application/json; charset=utf-8".toMediaTypeOrNull(), encryptedJSON.toString())
        return body
    }
}