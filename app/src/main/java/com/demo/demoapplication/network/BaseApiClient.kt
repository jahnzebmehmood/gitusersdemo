package com.theentertainerme.network.base

import com.delivery.dynamicdelivery.constants.ApiEndpoints
import com.demo.demoapplication.utils.ELog
import retrofit2.Retrofit


open class  BaseApiClient() {
    fun getClient(): Retrofit {
        return ApiFactory.Builder(ApiEndpoints.BASE_URL).build()
    }
}