package com.delivery.dynamicdelivery.network

import com.theentertainerme.network.base.BaseApiClient

object ApiClient : BaseApiClient() {
    private val authApiInterface: ApiInterface = getClient().create(ApiInterface::class.java)

    fun getApiService():ApiInterface{
        return authApiInterface
    }
}