package com.theentertainerme.network.base

import com.delivery.dynamicdelivery.storage.AppPreferencesStorage
import com.demo.demoapplication.utils.ELog
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit


class ApiFactory {


    class Builder(private val baseUrl: String,isJwtToken: Boolean=false) {

        private var headers: HashMap<String, String>? = null

        private fun retrofit(): Retrofit = Retrofit.Builder()
            .client(apiClient)
            .baseUrl(baseUrl)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()


        private val authInterceptor = Interceptor { chain ->
           // val apisBasicAuthInfo = ApisEncryptionUtils.getInstance().apiUserBasicCred
           // val auth = "Basic " + Base64.encodeToString(apisBasicAuthInfo.toByteArray(), Base64.URL_SAFE or Base64.NO_WRAP)
            var request = chain.request()
            request = request?.newBuilder()
                ?.addHeader("Accept","application/vnd.github.v3+json")
                ?.build()
           val response= chain.proceed(request)
            val rawJson = response.body?.string()
            ELog.logDebug("Response rawJson",rawJson)
            response.newBuilder()
                .body(rawJson?.let { ResponseBody.create(response.body?.contentType(), it) }).build()

        }

        //OkhttpClient for building http request url
        private val apiClient = OkHttpClient().newBuilder()
            .addInterceptor(authInterceptor)
            .addInterceptor(HttpLoggingInterceptor().apply {
//                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                level = HttpLoggingInterceptor.Level.NONE
            })
            .connectTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .build()


        var gson = GsonBuilder()
            .setLenient()
            .create()

        fun setHeaders(headers: HashMap<String, String>): Builder {
            this.headers = headers;
            return this
        }


        fun build(): Retrofit {
            return retrofit()
        }


    }


}