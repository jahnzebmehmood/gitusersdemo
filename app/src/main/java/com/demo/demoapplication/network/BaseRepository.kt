package com.theentertainerme.network.base

import com.delivery.dynamicdelivery.models.common.ErrorResponse
import com.delivery.dynamicdelivery.models.common.ResponseModel
import com.demo.demoapplication.R
import com.demo.demoapplication.controllers.AppClass
import com.demo.demoapplication.utils.ELog
import com.google.gson.Gson
import org.json.JSONObject
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException


open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): ResponseModel? {


        val result: BaseResult<T> = safeApiResult(call)
        var data: ResponseModel? = null

        when (result) {
            is BaseResult.Success ->
                data = ResponseModel(result.data,null)
            is BaseResult.Error -> {
                data = ResponseModel(null,result.errorResponse)

                //apiResponseListener.onError(result.errorResponse)
            }
        }

        return data

    }

    private suspend fun <T : Any> safeApiResult(call: suspend () -> Response<T>): BaseResult<T> {
        try {
            val response = call.invoke()
            if (response.isSuccessful) {
                ELog.logDebug("isSuccessful")
                return BaseResult.Success(response.body()!!)
            }
            ELog.logDebug("NotisSuccessful")
            if ( response.code() == 403 || response.code() == 401) {
                ELog.logDebug("NotisSuccessful 401")
//                if(UserSession.isLoggedIn()){
//                    ELog.logDebug("NotisSuccessful Logout")
//                    Utils.onLogOutNavigation()
//                }
            }
            try {
                val jObjError = JSONObject(response.errorBody()?.string())
                ELog.logDebug(jObjError.toString())
                val errorResponse = Gson().fromJson<ErrorResponse>(jObjError.toString(), ErrorResponse::class.java)
                return BaseResult.Error(errorResponse)
            } catch (exception: Exception) {
                exception.printStackTrace()
                val errorResponse = ErrorResponse()
                if (exception is UnknownHostException) {
                    errorResponse.message = AppClass.getInstance().getString(R.string.network_something_wrong)?:""
                    return BaseResult.Error(errorResponse)
                }
                if (response.code() in 500..510)
                    errorResponse.message = AppClass.getInstance().getString(R.string.network_error_message_502)?:""
                else if (response.code() in 401..403)
                    errorResponse.message = AppClass.getInstance().getString(R.string.network_error_message_403)?:""
                else
                    errorResponse.message = AppClass.getInstance().getString(R.string.network_something_wrong)?:""
                return BaseResult.Error(errorResponse)
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
            val errorResponse = ErrorResponse()
            if (exception is UnknownHostException) {
                errorResponse.message = AppClass.getInstance().getString(R.string.network_something_wrong)?:""
                return BaseResult.Error(errorResponse)
            }
            else if (exception is SocketTimeoutException){
                errorResponse.message = AppClass.getInstance().getString(R.string.network_error_message_time_out)?:""
                return BaseResult.Error(errorResponse)
            }
            errorResponse.message = AppClass.getInstance().getString(R.string.network_something_wrong)?:""
            return BaseResult.Error(errorResponse)
        }

    }


    interface ApiResponseListener {
        fun onSuccess(data: Any)
        fun onError(errorResponse: ErrorResponse)
    }
}