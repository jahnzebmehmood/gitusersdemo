package com.delivery.dynamicdelivery.network

import com.delivery.dynamicdelivery.constants.ApiEndpoints
import com.demo.demoapplication.models.gituser.GitHubUser
import com.demo.demoapplication.models.gituser.GitUserSearchResponse
import kotlinx.coroutines.Deferred
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface{

    @GET(ApiEndpoints.USERS)
    fun getGitHubUsersAsync(
        @Query("since") pageNo: Int = 0,
        @Query("per_page") pageSize: Int = 50
    ): Deferred<Response<ArrayList<GitHubUser>>>

    @GET(ApiEndpoints.USERS+"/{username}")
    fun getGitHubUserAsync(@Path("username") userName: String): Deferred<Response<GitHubUser>>

    @GET(ApiEndpoints.SEARCH)
    fun searchGitHubUsersAsync(
        @Query("q") query: String = "",
        @Query("page") pageNo: Int = 0,
        @Query("per_page") pageSize:Int = 50
    ): Deferred<Response<GitUserSearchResponse>>
}