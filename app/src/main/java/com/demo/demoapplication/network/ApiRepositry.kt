package com.delivery.dynamicdelivery.network

import com.delivery.dynamicdelivery.models.common.ResponseModel
import com.demo.demoapplication.controllers.AppClass
import com.demo.demoapplication.models.gituser.GitHubUser
import com.theentertainerme.network.base.BaseRepository

object ApiRepositry : BaseRepository(){

    suspend fun getGitUsersList(pageIndex: Int = 0, pageSize: Int = 10):ResponseModel?{
        val response = safeApiCall { ApiClient.getApiService().getGitHubUsersAsync(pageIndex, pageSize).await()}
        return response
    }

    suspend fun getGitUserDetail(userName:String):ResponseModel?{
        val response = safeApiCall { ApiClient.getApiService().getGitHubUserAsync(userName).await()}
        return response
    }

    suspend fun searchGitUsersList(query: String,pageIndex: Int = 0, pageSize: Int = 10):ResponseModel?{
        val response = safeApiCall { ApiClient.getApiService().searchGitHubUsersAsync(query,pageIndex, pageSize).await()}
        return response
    }

    fun getGitUsersListFromDb():ArrayList<GitHubUser>{
        return AppClass.getRoomDataBaseInstance(AppClass.getInstance()).gitHubUserDao().getAllUsers() as ArrayList<GitHubUser>
    }
}