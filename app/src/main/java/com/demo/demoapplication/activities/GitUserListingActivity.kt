package com.demo.demoapplication.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.delivery.dynamicdelivery.constants.AppContants
import com.demo.demoapplication.R
import com.demo.demoapplication.adapters.GitUsersListingAdapter
import com.demo.demoapplication.adapters.GitUsersPagingAdapter
import com.demo.demoapplication.adapters.ListingLoadStateAdapter
import com.demo.demoapplication.databinding.ActivityGitUsersListingBinding
import com.demo.demoapplication.interfaces.GitItemClickListener
import com.demo.demoapplication.models.gituser.GitHubUser
import com.demo.demoapplication.viewmodels.GitUsersListingViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList
import android.view.inputmethod.EditorInfo

import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState.NotLoading
import com.demo.demoapplication.connectivity.Connectivity
import com.demo.demoapplication.connectivity.InternetConnectionListener
import com.demo.demoapplication.controllers.AppClass
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.distinctUntilChanged


class GitUserListingActivity : AppCompatActivity(), GitItemClickListener {

    private var binding: ActivityGitUsersListingBinding? = null
    private var gitUsersListingViewModel: GitUsersListingViewModel? = null
    private var gitUsersListingAdapter: GitUsersListingAdapter? = null
    private var gitUsersPagingAdapter: GitUsersPagingAdapter? = null
    private var gitUsersListing: ArrayList<GitHubUser>? = null
    private var internetStatusReceiver: InternetConnectionListener.OnInternetConnectionListener? = null

    @ExperimentalPagingApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBinding()
        setViewModel()
        setViews()
        setAdapter()
//        setObservers()
//        callGitUsersFromNetwork()
        registerInternetReceiver()
        callGitUsersWithPaging()
    }

    private fun setBinding(){
        binding = ActivityGitUsersListingBinding.inflate(layoutInflater)
        setContentView(binding?.root)
    }

    private fun setViewModel(){
        gitUsersListingViewModel = ViewModelProvider(this).get(GitUsersListingViewModel::class.java)
    }

    @ExperimentalPagingApi
    private fun setViews(){
        binding?.swipeRefresh?.setOnRefreshListener {
            callGitUsersWithPaging()
        }
        binding?.etSearch?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                filter(s.toString())
            }

        })
        binding?.etSearch?.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                searchGitUsers()
                return@OnEditorActionListener true
            }
            false
        })
        binding?.etSearch?.setOnClickListener {
            startActivity(Intent(this,GitUserSearchActivity::class.java))
        }
    }

    private fun setAdapter(){
        gitUsersListingAdapter = GitUsersListingAdapter(this)
        gitUsersListingAdapter?.gitItemClickListener = this
        binding?.rcvGitUsers?.apply {
            adapter = gitUsersListingAdapter
        }

        gitUsersPagingAdapter = GitUsersPagingAdapter()
        gitUsersPagingAdapter?.gitItemClickListener = this
        binding?.rcvGitUsers?.apply {
            layoutManager = LinearLayoutManager(this@GitUserListingActivity)
            setHasFixedSize(true)
            adapter = gitUsersPagingAdapter?.withLoadStateFooter(
                footer = ListingLoadStateAdapter { gitUsersPagingAdapter?.retry() }
            )
        }
    }

    private fun setObservers(){
        gitUsersListingViewModel?.apiResponse?.observe(this, Observer {
            binding?.swipeRefresh?.isRefreshing = false
            if (it is ArrayList<*>){
                gitUsersListing = it as ArrayList<GitHubUser>
                gitUsersListingAdapter?.gitListing = it as ArrayList<GitHubUser>
            }
        })
    }

    private fun callGitUsersFromNetwork(){
        binding?.swipeRefresh?.isRefreshing = true
        gitUsersListingViewModel?.gitUsersList()
    }

    @ExperimentalPagingApi
    private fun callGitUsersWithPaging(){
        lifecycleScope.launch {
            gitUsersListingViewModel?.loadUserFromDb(gitUsersListingViewModel?.getDefaultPageConfig()!!)?.distinctUntilChanged()?.collectLatest {
                gitUsersPagingAdapter?.submitData(it)
            }
        }

        gitUsersPagingAdapter?.addLoadStateListener { loadState ->

            if (loadState.refresh is LoadState.Loading){
                binding?.swipeRefresh?.isRefreshing = true
            }
            else{
                binding?.swipeRefresh?.isRefreshing = false

                // getting the error
                val error = when {
                    loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                    loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                    loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                    else -> null
                }
                error?.let {
                    Toast.makeText(this, it.error.message, Toast.LENGTH_SHORT).show()
                }?: kotlin.run {
                    if (gitUsersPagingAdapter?.itemCount == 0)
                    Toast.makeText(this, getString(R.string.github_api_limit_exceed), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @ExperimentalPagingApi
    private fun searchGitUsers(){
        when(binding?.etSearch?.text?.toString()?.trim()?.length!! > 0){
            true -> {
                lifecycleScope.launch {
                    gitUsersListingViewModel?.getSearchedList(binding?.etSearch?.text.toString().trim())?.collect {
                        gitUsersPagingAdapter?.submitData(it)
                    }
                }
            }
            false -> {
                callGitUsersWithPaging()
            }

        }
    }

    private fun filter(text: String?) {
        //        if (text?.length==0)return
        if (text?.length == 0){
            gitUsersListingAdapter?.gitListing = gitUsersListing!!
            return
        }
        val temp: ArrayList<GitHubUser> = ArrayList()
        for (d in gitUsersListing!!) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (text?.lowercase(Locale.getDefault())?.let {
                    d.login?.lowercase(Locale.getDefault())?.contains(
                        it
                    )
                }!!) {
                temp.add(d)
            }
        }
        //update recyclerview
        gitUsersListingAdapter?.gitListing = temp
    }

    override fun onItemClick(gitHubUser: GitHubUser?) {
        startActivity(Intent(this, GitUserDetailActivity::class.java).putExtra(AppContants.USER_NAME, gitHubUser?.login))
    }

    override fun updateNote(gitHubUser: GitHubUser?, position: Int) {
        lifecycleScope.launch(Dispatchers.IO) {
            AppClass.getRoomDataBaseInstance(this@GitUserListingActivity).gitHubUserDao().updateUser(gitHubUser!!)
            gitUsersListing = AppClass.getRoomDataBaseInstance(this@GitUserListingActivity).gitHubUserDao()
                .getAllUsersList() as ArrayList<GitHubUser>
        }
        gitUsersPagingAdapter?.notifyItemChanged(position)
    }

    @ExperimentalPagingApi
    private fun registerInternetReceiver() {
        internetStatusReceiver = object : InternetConnectionListener.OnInternetConnectionListener {
            override fun onConnectionChanged() {
                if (Connectivity.isConnected()) {
                    showSnackBar(getString(R.string.back_online))
                    callGitUsersWithPaging()
                }else{
                    showSnackBar(getString(R.string.network_error))
                }
            }
        }
        InternetConnectionListener.addInternetReceiver(internetStatusReceiver!!)
    }

    private fun unregisterInternetReceiver(internetStatusReceiver: InternetConnectionListener.OnInternetConnectionListener) {
        InternetConnectionListener.removeReceiver(internetStatusReceiver)
    }

    private fun showSnackBar(message: String){
        Snackbar.make(window.decorView.rootView, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        if (internetStatusReceiver != null)
            unregisterInternetReceiver(internetStatusReceiver!!)
        super.onDestroy()
    }

}