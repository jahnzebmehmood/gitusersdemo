package com.demo.demoapplication.activities

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.delivery.dynamicdelivery.constants.AppContants
import com.demo.demoapplication.adapters.GitUsersListingAdapter
import com.demo.demoapplication.controllers.AppClass
import com.demo.demoapplication.databinding.ActivityGitUsersSearchBinding
import com.demo.demoapplication.interfaces.GitItemClickListener
import com.demo.demoapplication.models.gituser.GitHubUser
import com.demo.demoapplication.viewmodels.GitUsersListingViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class GitUserSearchActivity: AppCompatActivity(), GitItemClickListener {
    private var binding: ActivityGitUsersSearchBinding? = null
    private var gitUsersListingViewModel: GitUsersListingViewModel? = null
    private var gitUsersListingAdapter: GitUsersListingAdapter? = null
    private var gitUsersListing: ArrayList<GitHubUser>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBinding()
        setViewModel()
        setViews()
        setAdapter()
    }

    private fun setBinding(){
        binding = ActivityGitUsersSearchBinding.inflate(layoutInflater)
        setContentView(binding?.root)
    }

    private fun setViewModel(){
        gitUsersListingViewModel = ViewModelProvider(this).get(GitUsersListingViewModel::class.java)
    }

    private fun setViews() {
        binding?.etSearch?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (gitUsersListing?.isNotEmpty() == true)
                filter(s.toString())
            }

        })
    }

    private fun setAdapter(){
       lifecycleScope.launchWhenCreated {
           gitUsersListing =  AppClass.getRoomDataBaseInstance(this@GitUserSearchActivity).gitHubUserDao().getAllUsersList() as ArrayList<GitHubUser>
           gitUsersListingAdapter = GitUsersListingAdapter(this@GitUserSearchActivity)
           gitUsersListingAdapter?.gitItemClickListener = this@GitUserSearchActivity
           binding?.rcvGitUsers?.apply {
               adapter = gitUsersListingAdapter
           }
           gitUsersListingAdapter?.gitListing = gitUsersListing!!
        }
    }

    private fun filter(text: String?) {
        //        if (text?.length==0)return
        if (text?.length == 0){
            gitUsersListingAdapter?.gitListing = gitUsersListing!!
            return
        }
        val temp: ArrayList<GitHubUser> = ArrayList()
        for (d in gitUsersListing!!) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (text?.lowercase(Locale.getDefault())?.let {
                    d.login?.lowercase(Locale.getDefault())?.contains(
                        it
                    )
                }!!) {
                temp.add(d)
            }
        }
        //update recyclerview
        gitUsersListingAdapter?.gitListing = temp
    }

    override fun onItemClick(gitHubUser: GitHubUser?) {
        startActivity(Intent(this, GitUserDetailActivity::class.java).putExtra(AppContants.USER_NAME, gitHubUser?.login))
    }

    override fun updateNote(gitHubUser: GitHubUser?, position: Int) {
        lifecycleScope.launch(Dispatchers.IO) {
            AppClass.getRoomDataBaseInstance(this@GitUserSearchActivity).gitHubUserDao().updateUser(gitHubUser!!)
            gitUsersListing = AppClass.getRoomDataBaseInstance(this@GitUserSearchActivity).gitHubUserDao()
                .getAllUsersList() as ArrayList<GitHubUser>
        }
        gitUsersListingAdapter?.notifyItemChanged(position)
    }

}