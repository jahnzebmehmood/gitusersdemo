package com.demo.demoapplication.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.delivery.dynamicdelivery.constants.AppContants
import com.demo.demoapplication.R
import com.demo.demoapplication.databinding.ActivityGitUserDetailsBinding
import com.demo.demoapplication.models.gituser.GitHubUser
import com.demo.demoapplication.viewmodels.GitUserDetailViewModel
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso

class GitUserDetailActivity: AppCompatActivity() {

    private var binding: ActivityGitUserDetailsBinding? = null
    private var gitUserDetailViewModel: GitUserDetailViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBinding()
        setViewModel()
        setObserver()
        callGitUserDetailApi()
        setClickListeners()
    }

    private fun setBinding(){
        binding = ActivityGitUserDetailsBinding.inflate(layoutInflater)
        setContentView(binding?.root)
    }

    private fun setViewModel(){
        gitUserDetailViewModel = ViewModelProvider(this).get(GitUserDetailViewModel::class.java)
    }

    private fun setViews(gitHubUser: GitHubUser){
        Picasso.get().load(gitHubUser.avatarUrl).into(binding?.ivProfile)
        Picasso.get().load(gitHubUser.avatarUrl).into(binding?.ivHeaderProfile)
        binding?.tvTitle?.text = getString(R.string.name, gitHubUser.name)
        binding?.tvCompany?.text = getString(R.string.company, gitHubUser.company)
        binding?.tvBlog?.text = getString(R.string.blog, gitHubUser.blog)
        binding?.tvFollowers?.text = getString(R.string.followers, gitHubUser.followers?.toString())
        binding?.tvFollowing?.text = getString(R.string.following, gitHubUser.following?.toString())

    }

    private fun setObserver(){
        gitUserDetailViewModel?.apiResponse?.observe(this, Observer {
            binding?.swipeRefresh?.isRefreshing = false
            if (it is GitHubUser){
               setViews(it)
            }
        })
    }

    private fun callGitUserDetailApi(){
        binding?.swipeRefresh?.isRefreshing = true
        intent?.getStringExtra(AppContants.USER_NAME)?.let {
            gitUserDetailViewModel?.gitUserDetail(
                it
            )
        }
    }

    private fun setClickListeners(){
        binding?.ivBack?.setOnClickListener {
            finish()
        }
    }
}