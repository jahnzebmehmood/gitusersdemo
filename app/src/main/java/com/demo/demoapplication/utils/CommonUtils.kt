package com.delivery.dynamicdelivery.utils

import android.text.TextUtils
import android.util.Patterns


object CommonUtils {
    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
}