package com.delivery.dynamicdelivery.utils

import android.Manifest
import android.app.Activity
import android.app.PendingIntent
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Build.*
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import java.text.SimpleDateFormat
import java.util.*


object Utils {

    fun getSystemStatusBarSize(context: Context): Int {
        var pixelSize = 0
        val resId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resId > 0) {
            pixelSize = context.resources.getDimensionPixelSize(resId)
        }
        return pixelSize
    }

    fun getDeviceUDID(context: Context): String {
        try {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.CUPCAKE) {
                Settings.Secure.getString(
                    context.contentResolver,
                    Settings.Secure.ANDROID_ID
                )
            } else {
                return ""
            }
        } catch (ex: Exception) {
            return ""
        }

    }

    fun doCall(context: Context, phoneNumber: String?) {
        phoneNumber?.let {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$phoneNumber")
            context.startActivity(intent)
        }

    }

    fun getLast7DaysFromCurrent(currentDate: Date): Date {
        val cal = GregorianCalendar.getInstance()
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_YEAR, -7);
        return cal.getTime();
    }

    fun getLast1DaysFromCurrent(currentDate: Date): Date {
        val cal = GregorianCalendar.getInstance()
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_YEAR, -1);
        return cal.getTime();
    }


    fun getCurrentDateInHistoryFormat():String{
        //1 July, 2020
        val dateExpected = SimpleDateFormat("dd MMM, YYYY")
        return dateExpected.format(Date())
    }

    fun getCurrentDateInAPIFormat(currentDate: Date):String{
        //YYYY-MM-DD
        val dateExpected = SimpleDateFormat("yyyy-MM-dd")
        return dateExpected.format(currentDate)
    }

    fun getLast7FromCurrentAPIFormat(currentDate: Date):Pair<String,Date>{
        //YYYY-MM-DD
        val dateExpected = SimpleDateFormat("yyyy-MM-dd")
        val currentLast7Days = getLast7DaysFromCurrent(currentDate)
        val currentDateInString = dateExpected.format(currentLast7Days)
        return Pair(currentDateInString,currentLast7Days)
    }

    fun getCurrentWeeklyTitleFormat():String{
        //1 July, 2020
        val dateFromExpected = SimpleDateFormat("dd MMM")
        val dateToExpected = SimpleDateFormat("dd MMM, YYYY")
        val daysBeforeDate = getLast7DaysFromCurrent(Date())
        val fromDate = dateFromExpected.format(daysBeforeDate)
        val toDate = dateToExpected.format(Date())
        return fromDate + "-" + toDate
    }

}