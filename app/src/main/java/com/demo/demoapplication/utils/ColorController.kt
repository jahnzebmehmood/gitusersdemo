package com.delivery.dynamicdelivery.utils

import android.graphics.Color

class ColorController {


    companion object{
        fun parseStringColor(colorString: String?): Int {
            var colorString = colorString
            if (colorString != null) {
                if (!colorString.startsWith("#"))
                    colorString = "#$colorString"
                try {
                    return Color.parseColor(colorString.trim { it <= ' ' })
                } catch (numberFormatException: NumberFormatException) {
                    return 0
                } catch (parseException: IllegalArgumentException) {
                    return 0
                }

            }
            return 0
        }

        fun returnHexCode(colorString: String?):String{
            var colorString = colorString
            if (colorString != null) {
                if (!colorString.startsWith("#"))
                    colorString = "#$colorString"
            }
            return colorString?:"#ffffff"
        }

        fun manipulateColor(color: Int, factor: Float): Int {
            val a = Color.alpha(color)
            val r = Math.round(Color.red(color) * factor)
            val g = Math.round(Color.green(color) * factor)
            val b = Math.round(Color.blue(color) * factor)
            return Color.argb(a,
                    Math.min(r, 255),
                    Math.min(g, 255),
                    Math.min(b, 255))
        }
    }

}